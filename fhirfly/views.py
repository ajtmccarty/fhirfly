import logging

from django.views.generic import View
from django.conf import settings
from django.http import HttpResponseBadRequest
from django.shortcuts import render, redirect, reverse
from django.core.exceptions import ImproperlyConfigured
from request_fhirclient import RequestFHIRClient
from fhirclient.models import medicationstatement, fhirsearch
from .models import Patient

logger = logging.getLogger(__name__)

class LoggingView(View):
    
    def dispatch(self, request, *args, **kwargs):
        logger.info("Incoming request to: {}".format(request.build_absolute_uri()))
        try:
            response = super(LoggingView, self).dispatch(request, *args, **kwargs)
        except Exception as e:
            logger.critical("{e_type}: {e_msg}".format(e_type=e.__class__, e_msg=e))
            raise e
        if hasattr(response, "url"):
            logger.info("Returning to {}".format(response.url))
        return response


class LogoView(View):

    def get(self, request, **kwargs):
        return render(request, "fhirfly/logo.html")


class LaunchView(LoggingView):

    def get(self, request, *args, **kwargs):
        # where this request says it is coming from
        issuer = request.GET.get("iss")
        if not issuer:
            return HttpResponseBadRequest("No 'iss' received in GET parameters")
        # the settings for this FHIR server
        ehr_settings = None
        for ehr_details in settings.EHR_SPECIFIC.values():
            if ehr_details["iss"] == issuer:
                ehr_settings = ehr_details["settings"]
                break
        if not ehr_settings:
            msg = "Issuer '{iss}' is not registered with fhirfly"
            return HttpResponseBadRequest(msg.format(iss=issuer))
        smart = RequestFHIRClient(request, state_get_params=["iss", "launch"], state_settings=ehr_settings, reset_state=True)
        smart.save()
        return redirect(smart.authorize_url)


class CallbackView(LoggingView):

    def get(self, request, *args, **kwargs):
        smart = RequestFHIRClient(request)
        smart.handle_callback()
        context = smart.prepare_context()
        p_id = smart.get_launch_patient_id()
        if p_id:
            context["patient"] = smart.get_fhir_obj(Patient, p_id)
            #search = fhirsearch.FHIRSearch(medicationstatement.MedicationStatement, {"status": "active"})
            #context["med_statement"] = search.perform_resources(smart)
        return render(request, "fhirfly/index.html", context=context)
        

class WebEntryView(LoggingView):
    
    def get(self, request, **kwargs):
        context = self.get_context_data()
        return render(request, "fhirfly/web_entry.html", context=context)
        
    def get_context_data(self, **kwargs):
        if hasattr(super(WebEntryView, self), "get_context_data"):
            context = super(WebEntryView, self).get_context_data(**kwargs)
        else:
            context = {}
        context["ehr_options"] = []
        base_url = reverse("authorize")
        for ehr_key, ehr_specifics in settings.EHR_SPECIFIC.items():
            if "aud" not in ehr_specifics["settings"]:
                msg = "In settings.EHR_SPECIFIC '{key}' needs a value for 'aud', it should be the URL of the FHIR server"
                raise ImproperlyConfigured(msg.format(key=ehr_key))
            context["ehr_options"].append({"title": ehr_specifics["title"],
                                           "url": base_url + "?ehr_key=" + ehr_key})
        return context
    
class AuthorizeView(LoggingView):
    
    def get(self, request, **kwargs):
        ehr_key = request.GET.get("ehr_key")
        ehr_settings = settings.EHR_SPECIFIC[ehr_key]["settings"]
        ehr_settings["iss"] = settings.EHR_SPECIFIC[ehr_key]["iss"]
        smart = RequestFHIRClient(request, state_settings=ehr_settings, reset_state=True)
        smart.save()
        if not settings.EHR_SPECIFIC[ehr_key]["auth_required"]:
            return render(request, "fhirfly/index.html")
        return redirect(smart.authorize_url)
        

class DocumentationView(LoggingView):
    
    template_name = "fhirfly/documentation.html"
    
    def get(self, request, **kwargs):
        return render(request, self.template_name)
