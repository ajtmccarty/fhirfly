import os


def get_setting(name):
    try:
        from . import localsettings
        return getattr(localsettings, name)
    except (ImportError, AttributeError):
        envvar = os.environ.get(name)
        if envvar is not None and "|" in envvar:
            return envvar.split("|")
        return envvar


def build_get_params(start_params):
        """
        Turn a dict into a string of GET params for a URL request
        
        Uses values in the start_params dict to create the GET params.
        Can handle sequences as values, they will be joined with "+"
        If a key in the dict lacks a value, will raise a ValueError
        
        Turns a dict into the form
            "&key=value&key=value_sub_1+value_sub_2"
        
        :param dict start_params: dict of parameter names and values
        :return str: returns the GET string
        """
        param_list = []
        for param, val in start_params.items():
            param = str(param)
            # if it's None, raise an error
            if val is None or len(val) == 0:
                msg = "No value for key {} in 'start_params'"
                raise ValueError(msg.format(param))
            # if the val isn't a list or a string, ignore it
            elif isinstance(val, str) or isinstance(val, unicode):
                param_list.append(param + "=" + val)
            # if this is some sort of sequence
            elif hasattr(val, '__iter__'):
                param_list.append(param + "=" + "+".join(val))
            else:
                msg = "Value '{val}' for key '{key}' cannot be turned into a string"
                raise ValueError(msg.format(val=val, key=param))
        return "&".join(param_list)