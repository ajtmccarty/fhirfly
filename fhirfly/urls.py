"""fhirfly URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^logo/', views.LogoView.as_view(), name='logo'),
    url(r'^fhir-app/launch(\.html)?$', views.LaunchView.as_view(), name='launch'),
    url(r'^fhir-app/$', views.CallbackView.as_view(), name='callback'),
    url(r'^authorize/$', views.AuthorizeView.as_view(), name='authorize'),
    url(r'^docs/$', views.DocumentationView.as_view(), name='docs'),
    url(r'^$', views.WebEntryView.as_view(), name='web_entry'),
]
