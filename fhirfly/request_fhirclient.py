import requests
import random
import string
import jwt
import logging
from urlparse import urlparse
from datetime import timedelta

from django.utils import timezone

from .helpers import get_setting, build_get_params

logger = logging.getLogger(__name__)

class RequestFHIRClient(object):
    
    # class-level tuples with required and allowed parameters for different 
    # steps in auth process authentication
    allowed_get_params = ("iss", "launch", "code", "state")
    allowed_post_params = ()
    allowed_access_params = ("access_token", "token_type", "expires_in",
                             "scope", "id_token", "refresh_token")
    required_access_params = ("token_type", "access_token", "scope")
    allowed_launch_context_params = ("patient", "encounter", "location",
                                     "need_patient_banner", "resource",
                                     "intent", "smart_style_url")

    def __init__(self, request, state_get_params=None, state_post_params=None, state_settings=None, reset_state=False):
        """
        Initialize the FHIRClient and link to the request session
        Uses state_get_params and/or state_post_params to record parameters from the request
        Needs to have one of state_get_params, state_post_params, state_settings
        
        :param HttpRequest request: the request
        :param list state_get_params: GET parameters to save from the request
        :param list state_post_params: POST parameters to save from the request
        :param dict state_settings: overrides anything coming in from the request
                                    and is saved in self.state
        :param bool reset_state: if truthy, clear out state saved in the session
        """
        self.request = request
        # get the state from the session
        self.state = self.request.session.get("fhirstate")
        # if nothing in session, set it to an empty dict and link it with self.state
        if (self.state is None) or reset_state:
            self.request.session["fhirstate"] = {}
            self.state = self.request.session["fhirstate"]
        # make sure we get one of the setting arguments or have state saved
        if not(state_get_params or state_post_params or state_settings or self.state):
            msg = "Must initialize with one of [state_get_params, state_post_params," \
                  " or state_settings] or have saved state"
            raise RequestFHIRClientError(msg)
        # look at the request GET and/or POST params
        if state_get_params:
            for param in state_get_params:
                # only allow whitelisted parameters
                if param in self.allowed_get_params:
                    try:
                        self.state[param] = self.request.GET[param]
                        if param == "iss":
                            self.state["aud"] = self.state["iss"]
                    except KeyError:
                        pass
        if state_post_params:
            for param in state_post_params:
                # only allow whitelisted parameters
                if param in self.allowed_post_params:
                    try:
                        self.state[param] = self.request.POST[param]
                    except KeyError:
                        pass
        # Override params from request using settings
        if state_settings:
            if not isinstance(state_settings, dict):
                raise RequestFHIRClientError("'state_settings' must be a dict")
            for param in state_settings:
                self.state[param] = state_settings[param]

    def save(self, param=None):
        """
        Save the state of the client to the request session
        
        Saves the entire state or a single key's value if param is set
        :param param: key in self.state
        """
        # try to save just one key if param is set
        if param:
            try:
                self.request.session["fhirstate"][param] = self.state[param]
            except KeyError:
                msg = "Failed to save parameter '{0}' to the session"
                raise RequestFHIRClientError(msg.format(param))
        # otherwise save the whole thing
        else:
            self.request.session["fhirstate"] = self.state

    def prepare_context(self):
        """Return a dictionary for use in a view"""
        context = {}
        # mapping from self.state key to context key
        context_state_dict = {
            "fhiruser": "user",
            "launch_context": "launch_context"
        }
        for context_key, state_key in context_state_dict.items():
            if state_key in self.state:
                # use a copy of the value so the true value can't be altered
                context[context_key] = self.state[state_key].copy()
        return context

    def get_conformance(self):
        """
        Try to retrieve and save the FHIR server's conformance statement
        
        Saves the conformance statement into self.state if retrieved
        """
        if self.state.get("conformance"):
            return
        try:
            base_url = self.state["iss"]
        except KeyError:
            raise RequestFHIRClientError("'iss' must be defined to retrieve the conformance statement")
        c_url = base_url + "/metadata"
        response = requests.get(c_url, headers={"ACCEPT": "application/json"})
        if response.status_code != 200:
            raise RequestFHIRClientError("request to " + c_url + " for conformance statement returned an error")
        self.state["conformance"] = response.json()

    def get_auth_endpoint(self, endpoint):
        """
        Get an authorization endpoint from the (rest, security) section of the conformance statement
        
        Try to get the auth_endpoint from self.state
        If not there, make sure we have the conformance statement in self.state
        Then search the endpoints for the input endpoint
        
        :param str endpoint: string to find an endpoint for ("token", "authorize", others)
        :return str: URL of the endpoint in question
        """
        # try to get a saved endpoint
        try:
            return self.state["auth_endpoints"][endpoint]
        except KeyError:
            pass
        # get the conformance statement
        if not self.state.get("conformance"):
            self.get_conformance()
        # try to get the requested auth endpoint
        try:
            endpoints = self.state["conformance"]["rest"][0]["security"]["extension"][0]["extension"]
            for ep in endpoints:
                if ep["url"] == endpoint:
                    if self.state.get("auth_endpoints") is None:
                        self.state["auth_endpoints"] = {}
                    self.state["auth_endpoints"][endpoint] = ep["valueUri"]
                    self.save("auth_endpoints")
                    return ep["valueUri"]
        # if this doesn't work then the conformance statement is not correctly formatted
        # for OAuth
        except (KeyError, IndexError):
            pass
        raise RequestFHIRClientError("conformance statement is not of expected format")

    def _generate_state(self, num_chars=15):
        """Get a pseudo-random string of ascii letters of the given length and save it"""
        random_string = "".join([random.choice(string.ascii_letters) for x in range(num_chars)])
        self.state["state"] = random_string
        self.save("state")

    @property
    def authorize_url(self):
        """
        Returns the URL to request authorization according to this step in
        the handshake 
        <http://docs.smarthealthit.org/authorization/#app-asks-for-authorization>
        
        :return str: the URL to use for requesting access
        """
        base_url = self.get_auth_endpoint("authorize")
        self._generate_state()
        ask_for_auth_settings = {
            "response_type": "code",
            "client_id": self.state.get("client_id"),
            "redirect_uri": get_setting("REDIRECT_URI"),
            "scope": self.state.get("scopes") or get_setting("SCOPES"),
            "state": self.state.get("state"),
            "aud": self.state.get("aud")
        }
        if self.state.get("launch"):
            ask_for_auth_settings["launch"] = self.state["launch"]
        payload = build_get_params(ask_for_auth_settings)
        return base_url + "?" + payload

    def handle_id(self, openid):
        """
        Work In Progress
        
        Reads the input openid Javascript Web Token without verifying the signature
        and saves the relevant user info, noting that it is not fully authenticated
        
        Does verify the following:
            "iss" (issuer) claim matches the conformance statement "token" endpoint
            "aud" (audience) claim is a single item and it is the CLIENT_ID
        
        """
        claims = jwt.decode(openid, verify=False)
        ### KEY IS NOT VERIFIED ###
        # verify claims see (http://openid.net/specs/openid-connect-basic-1_0-28.html#id_token)
        if claims.get("iss") is None:
            raise RequestFHIRClientError("No 'iss' claim received as part of ID token")
        parsed_token = urlparse(self.get_auth_endpoint("token"))
        parsed_iss = urlparse(claims["iss"])
        if parsed_token.netloc != parsed_iss.netloc:
            msg = "Received 'iss' claim ('{0}') does not have the same base as the token endpoint '{1}'"
            raise RequestFHIRClientError(msg.format(claims["iss"], self.get_auth_endpoint("token")))
        if claims.get("aud") is None:
            raise RequestFHIRClientError("No 'aud' claim received as part of ID Token")
        if isinstance(claims["aud"], list):
            if len(claims["aud"]) > 1:
                msg = "'aud' claim holds more than 1 value ({0}). Expect a single value."
                raise RequestFHIRClientError(msg.format(claims["aud"]))
            aud = claims["aud"][0]
        else:
            aud = claims["aud"]
        if aud != self.state.get("client_id"):
            msg = "Received 'aud' claim ('{0}') does not match Client ID"
            raise RequestFHIRClientError(msg.format(aud))
        self.record_profile(claims, authenticated=False)
        '''
        # openid info
        response = requests.get(self.state["iss"] + "/.well-known/openid-configuration", headers=headers)
        '''

    def record_profile(self, jwt_claims, authenticated=False):
        """
        Saves the user profile to self.state["user"]
        
        :param dict jwt_claims: Javascript Web Token claims in a dict
        :param bool authenticated: optional, default False, flag indicating if the user JWT
                                   has been cryptographically verified
        """
        # set the user value in self.state to an empty dict if it doesn't exist
        if not self.state.get("user"):
            self.state["user"] = {}
        self.state["user"]["authenticated"] = authenticated
        # only saves these claims from the JWT
        claims_to_save = ["profile", "sub"]
        for c in claims_to_save:
            if c in jwt_claims:
                self.state["user"][c] = jwt_claims[c]
        self.save("user")

    def handle_callback(self):
        """
        Handles retrieving an Access Token from the FHIR auth server
        
        Expectations specified here
        <http://docs.smarthealthit.org/authorization/#app-exchanges-authorization-code-for-access-token>
        
        """
        # make sure the 'state' param returned by the FHIR server is correct
        # note 'state' here is different from self.state, it should be the value
        # created in self.generate_state
        try:
            received_state = self.request.GET["state"]
        except KeyError:
            raise RequestFHIRClientError("Did not receive a state parameter as part of the FHIR server return")
        if received_state != self.state["state"]:
            msg = "Received state does not match expected value. Expected '{0}', received '{1}'"
            raise RequestFHIRClientError(msg.format(self.state["state"], received_state))
        # record the 'code' from the request to return to the FHIR server
        try:
            auth_code = self.request.GET["code"]
        except KeyError:
            raise RequestFHIRClientError("Did not receive a 'code' value as part of the FHIR server return")
        # build the return to the FHIR server
        base_url = self.get_auth_endpoint("token")
        params = {
            "grant_type": "authorization_code",
            "code": auth_code,
            "redirect_uri": get_setting("REDIRECT_URI"),
            "client_id": self.state.get("client_id")
        }
        # POST the return to the FHIR server
        response = requests.post(base_url, params)
        # verify the headers as specified in the above URL
        headers = response.headers
        if "cache-control" not in headers or headers["cache-control"].lower() != "no-store":
            msg = "Authorization server return did not have a HTTP 'Cache-Control' header with a value of 'no-store'"
            raise RequestFHIRClientError(msg)
        if "pragma" not in headers or headers["pragma"].lower() != "no-cache":
            msg = "Authorization server return did not have a 'Pragma' header with a value of 'no-cache'"
            raise RequestFHIRClientError(msg)
        # get the response into a dict
        response_dict = response.json()
        # if we got an error back in the response
        if "error" in response_dict:
            msg = "Unable to authenticate. Received error_code '{0}'".format(response_dict["error"])
            if "error_description" in response_dict:
                msg += " with message '{0}'".format(response_dict["error_description"])
            raise RequestFHIRClientError(msg)
        # if we got an HTTP error back with no error code
        if response.status_code != 200:
            msg = "Error when posting to token endpoint '{0}', returned HTTP status {1}"
            raise RequestFHIRClientError(msg.format(base_url, response.status_code))
        # if we are missing any required params from the response
        missing_params = []
        for param in self.required_access_params:
            if param not in response_dict:
                missing_params.append(param)
        if missing_params:
            msg = "These parameters were expected in the token response, but not received: {0}"
            raise RequestFHIRClientError(msg.format(missing_params))
        # otherwise, the response was good
        self.state["granted_access"] = {}
        self.state["launch_context"] = {}
        unexpected_params = {}
        for param in response_dict:
            if param in self.allowed_access_params:
                # convert the 'expires_in' to a datetime
                if param == "expires_in":
                    exp_time = timezone.now() + timedelta(seconds=response_dict["expires_in"])
                    self.state["granted_access"]["expiration_time"] = exp_time
                # split the 'scope' into a list
                elif param == "scope":
                    granted_scopes = response_dict["scope"].split(" ")
                    self.state["granted_access"]["scope"] = granted_scopes
                # handle verifying and reading the id_token, if we got one
                elif param == "id_token":
                    self.handle_id(response_dict["id_token"])
                else:
                    self.state["granted_access"][param] = response_dict[param]
            elif param in self.allowed_launch_context_params:
                self.state["launch_context"][param] = response_dict[param]
            # if anything unexpected comes back, raise an error
            else:
                unexpected_params[param] = response_dict[param]
        if unexpected_params:
            msg = "Received unexpected parameters {unexpected}"
            logger.warning(msg.format(unexpected=unexpected_params))
        self.save("granted_access")
        self.save("launch_context")

    def get_launch_patient_id(self):
        """Get the launch context patient ID if it exists"""
        try:
            return self.state["launch_context"]["patient"]
        except KeyError:
            return None

    def get_fhir_obj(self, resource, r_id):
        """
        Read a given FHIR object with the given ID from the FHIR server
        
        Intended to be used with a sub-class of fhirclient.models.fhirabstractresource
        The fhirabstractresource uses the self instance to self.request_json with
        the appropriate URL
        
        :param fhirclient.models.fhirabstractresource resource: model class to retrieve
        :param str r_id: ID of the resource to retrieve
        """
        return resource.read(r_id, self)

    def request_json(self, path):
        """
        Sends a request to the FHIR server at the given path with appropriate authorization
        
        :param str path: path to append to base_url
        :return dict: dict version of the returned JSON
        """
        headers = {
            "Authorization": "Bearer {}".format(self.state["granted_access"]["access_token"]),
            "Accept": "application/json"
        }
        base_url = self.state["iss"]
        if base_url[-1] != "/":
            base_url += "/"
        return requests.get(base_url + path, headers=headers).json()

class RequestFHIRClientError(Exception):
    pass