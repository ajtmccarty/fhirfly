from fhirclient.models import patient, address

class Patient(patient.Patient):
    
    @property
    def home_addr(self):
        home = self.get_addr_by_use("home")
        return Address.addr_str(home)
    
    def get_addr_by_use(self, addr_use):
        allowed_uses = ["home", "work", "temp", "old"]
        if addr_use not in allowed_uses:
            msg = "Address Use '{use}' invalid. Allowed values {allowed}"
            raise ValueError(msg.format(use=addr_use, allowed=allowed_uses))
        if not hasattr(self, "address"):
            return None
        for addr in self.address:
            if addr.use.lower() == addr_use:
                return addr
        return None


class Address(object):

    @staticmethod
    def addr_str(addr_obj):
        addr_frame = "{line}, {city}, {state} {postalCode}"
        f_dict = {attr: getattr(addr_obj, attr, "UNK") 
                  for attr in ["city", "state", "postalCode"]}
        lines = getattr(addr_obj, "line", "UNK")
        if isinstance(lines, list):
            f_dict["line"] = " ".join(lines)
        else:
            f_dict["line"] =  lines
        return addr_frame.format(**f_dict)    
